#!/usr/bin/env python
# coding: utf-8

# In[1]:


import glob
import pandas as pd
import urllib.request
import requests
import time
from zipfile import ZipFile 
import wget
import os
from urllib.parse import urlsplit, urlunsplit
import scipy


# In[2]:


# Import some modules etc
import json
import textwrap
import sklearn
from sklearn.model_selection import train_test_split  # Splits data in 2
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from sklearn.pipeline import Pipeline
from sklearn.datasets import fetch_openml
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
from sklearn.linear_model import LinearRegression
from sklearn.naive_bayes import BernoulliNB
from sklearn.naive_bayes import MultinomialNB
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics import accuracy_score
from sklearn.naive_bayes import MultinomialNB
from urllib.parse import urlsplit, urlunsplit

# 'Reproducable' randommness.
np.random.seed(24)


# In[3]:


html = urllib.request.urlopen("http://data.gdeltproject.org/gdeltv2/masterfilelist.txt").readlines()


# In[4]:


len(html)


# In[5]:


pol_unrest = """revolt
revolution
rebellion
rebels
insurgents
protest
protests
"""


# In[6]:


labor = """slave
enslaved
servitude
captivity
enslave
enslaved
"""


# In[7]:


bilabor = """modern slavery
human trafficking
forced labor
supply chain
modern slaves
modern slave
"""


# In[8]:


reject = """
star
fashion
shaped
"""


# In[9]:


table_cols = ['globaleventid', 'day', 'monthyear', 'year', 'fractiondate',
       'actor1code', 'actor1name', 'actor1countrycode', 'actor1knowngroupcode',
       'actor1ethniccode', 'actor1religion1code', 'actor1religion2code',
       'actor1type1code', 'actor1type2code', 'actor1type3code', 'actor2code',
       'actor2name', 'actor2countrycode', 'actor2knowngroupcode',
       'actor2ethniccode', 'actor2religion1code', 'actor2religion2code',
       'actor2type1code', 'actor2type2code', 'actor2type3code', 'isrootevent',
       'eventcode', 'eventbasecode', 'eventrootcode', 'quadclass',
       'goldsteinscale', 'nummentions', 'numsources', 'numarticles', 'avgtone',
       'actor1geo_type', 'actor1geo_fullname', 'actor1geo_countrycode',
       'actor1geo_adm1code', 'actor1geo_adm2code', 'actor1geo_lat',
       'actor1geo_long', 'actor1geo_featureid', 'actor2geo_type',
       'actor2geo_fullname', 'actor2geo_countrycode', 'actor2geo_adm2code',
       'actor2geo_adm2code.1', 'actor2geo_lat', 'actor2geo_long',
       'actor2geo_featureid', 'actiongeo_type', 'actiongeo_fullname',
       'actiongeo_countrycode', 'actiongeo_adm1code', 'actiongeo_adm2code',
       'actiongeo_lat', 'actiongeo_long', 'actiongeo_featureid', 'dateadded',
       'sourceurl']


# In[37]:


count = 0
newest = 618000
for line in html[470000:474000]:
    try:
        # Article Exraction --------------------------------

        print(count)
        url = line.split()[2].decode('UTF-8')
        print(url[len(url)-14:len(url)])
        if url[len(url)-14:len(url)] != 'export.CSV.zip':
            print('not csv', count)
            count += 1
            continue

        wget.download(url, 'E:/fr')

        # opening the zip file in READ mode 
        with ZipFile(url[37:], 'r') as zip: 
            # extracting all the files 
            print('Extracting all the files now...') 
            zip.extractall() 
            print('Done!') 

        # --------------- Pandas and Labeling Section ---------------
        d = pd.read_csv(url[37:-4], sep='\t', header=None)

        d.columns = table_cols

        url_titles = []
        base_url = []
        for i in d['sourceurl'].tolist():
            temp_list = []
            splits = i.split('-')
            temp_list.append(splits[0].split('/')[len(splits[0].split('/')) - 1])
            temp_list.extend(splits[1:])
            url_titles.append(" ".join(temp_list))
            split_url = urlsplit(i)
            base = split_url.netloc
            base_url.append(base)

            cu = []
        for i in range(len(url_titles)):
            cu.append(0)
        for i in range(len(url_titles)):
            for j in range(len(url_titles[i].split()) - 1):
                if url_titles[i].split()[j].lower() in pol_unrest.split():
                    cu[i] = 1
                    break
        
        pu = []
        for i in range(len(url_titles)):
            pu.append(0)
        for i in range(len(url_titles)):
            for j in range(len(url_titles[i].split()) - 1):
                if url_titles[i].split()[j].lower() in labor.split():
                    pu[i] = 1
                    break
                if url_titles[i].split()[j].lower() + ' ' + url_titles[i].split()[j+1].lower() in bilabor.split():
                    pu[i] = 1
                    break

        d['base_url'] = base_url
        d['title'] = url_titles
        d['civil_unrest'] = cu
        d['labor'] = pu
        d = d.drop_duplicates(subset = 'sourceurl')
        d = d.drop_duplicates(subset = 'base_url')
        d = d.drop_duplicates(subset = 'title')

        d = d[["globaleventid", "day", "actor1name", "actor2name", 'numarticles', "avgtone", "actor1geo_fullname", 'actor1geo_lat',
       'actor1geo_long', "actor2geo_fullname", "actiongeo_fullname", "sourceurl", 'base_url', 'title', 'civil_unrest', 'labor']]

        d.to_csv('E:/fr/474000:484000.csv', header = False, mode = 'a', index = False)
        print('successful')
#         os.remove(url[37:-4])
    except Exception as e:
        print(e)


# In[ ]:


count = 0
newest = 618000
for line in html[470000:474000]:
    try:
        # Article Exraction --------------------------------

        print(count)
        url = line.split()[2].decode('UTF-8')
        print(url[len(url)-14:len(url)])
        if url[len(url)-14:len(url)] != 'export.CSV.zip':
            print('not csv', count)
            count += 1
            continue

        wget.download(url, 'E:/fr')

        # opening the zip file in READ mode 
        with ZipFile(url[37:], 'r') as zip: 
            # extracting all the files 
            print('Extracting all the files now...') 
            zip.extractall() 
            print('Done!') 

        # --------------- Pandas and Labeling Section ---------------
        d = pd.read_csv(url[37:-4], sep='\t', header=None)

        d.columns = table_cols

        url_titles = []
        base_url = []
        for i in d['sourceurl'].tolist():
            temp_list = []
            splits = i.split('-')
            temp_list.append(splits[0].split('/')[len(splits[0].split('/')) - 1])
            temp_list.extend(splits[1:])
            url_titles.append(" ".join(temp_list))
            split_url = urlsplit(i)
            base = split_url.netloc
            base_url.append(base)

            cu = []
        for i in range(len(url_titles)):
            cu.append(0)
        for i in range(len(url_titles)):
            for j in range(len(url_titles[i].split()) - 1):
                if url_titles[i].split()[j].lower() in pol_unrest.split():
                    cu[i] = 1
                    break
        
        pu = []
        for i in range(len(url_titles)):
            pu.append(0)
        for i in range(len(url_titles)):
            for j in range(len(url_titles[i].split()) - 1):
                if url_titles[i].split()[j].lower() in labor.split():
                    pu[i] = 1
                    break
                if url_titles[i].split()[j].lower() + ' ' + url_titles[i].split()[j+1].lower() in bilabor.split():
                    pu[i] = 1
                    break

        d['base_url'] = base_url
        d['title'] = url_titles
        d['civil_unrest'] = cu
        d['labor'] = pu
        d = d.drop_duplicates(subset = 'sourceurl')
        d = d.drop_duplicates(subset = 'base_url')
        d = d.drop_duplicates(subset = 'title')

        d = d[["globaleventid", "day", "actor1name", "actor2name", 'numarticles', "avgtone", "actor1geo_fullname", 'actor1geo_lat',
       'actor1geo_long', "actor2geo_fullname", "actiongeo_fullname", "sourceurl", 'base_url', 'title', 'civil_unrest', 'labor']]

        d.to_csv('E:/fr/456000:460000.csv', header = False, mode = 'a', index = False)
        print('successful')
#         os.remove(url[37:-4])
    except Exception as e:
        print(e)


# In[10]:


count = 0
newest = 618000
for line in html[464000:470000]:
    try:
        # Article Exraction --------------------------------

        print(count)
        url = line.split()[2].decode('UTF-8')
        print(url[len(url)-14:len(url)])
        if url[len(url)-14:len(url)] != 'export.CSV.zip':
            print('not csv', count)
            count += 1
            continue

        wget.download(url, 'E:/fr')

        # opening the zip file in READ mode 
        with ZipFile(url[37:], 'r') as zip: 
            # extracting all the files 
            print('Extracting all the files now...') 
            zip.extractall() 
            print('Done!') 

        # --------------- Pandas and Labeling Section ---------------
        d = pd.read_csv(url[37:-4], sep='\t', header=None)

        d.columns = table_cols

        url_titles = []
        base_url = []
        for i in d['sourceurl'].tolist():
            temp_list = []
            splits = i.split('-')
            temp_list.append(splits[0].split('/')[len(splits[0].split('/')) - 1])
            temp_list.extend(splits[1:])
            url_titles.append(" ".join(temp_list))
            split_url = urlsplit(i)
            base = split_url.netloc
            base_url.append(base)

            cu = []
        for i in range(len(url_titles)):
            cu.append(0)
        for i in range(len(url_titles)):
            for j in range(len(url_titles[i].split()) - 1):
                if url_titles[i].split()[j].lower() in pol_unrest.split():
                    cu[i] = 1
                    break
        
        pu = []
        for i in range(len(url_titles)):
            pu.append(0)
        for i in range(len(url_titles)):
            for j in range(len(url_titles[i].split()) - 1):
                if url_titles[i].split()[j].lower() in labor.split():
                    pu[i] = 1
                    break
                if url_titles[i].split()[j].lower() + ' ' + url_titles[i].split()[j+1].lower() in bilabor.split():
                    pu[i] = 1
                    break

        d['base_url'] = base_url
        d['title'] = url_titles
        d['civil_unrest'] = cu
        d['labor'] = pu
        d = d.drop_duplicates(subset = 'sourceurl')
        d = d.drop_duplicates(subset = 'base_url')
        d = d.drop_duplicates(subset = 'title')

        d = d[["globaleventid", "day", "actor1name", "actor2name", 'numarticles', "avgtone", "actor1geo_fullname", 'actor1geo_lat',
       'actor1geo_long', "actor2geo_fullname", "actiongeo_fullname", "sourceurl", 'base_url', 'title', 'civil_unrest', 'labor']]

        d.to_csv('E:/fr/464000:470000.csv', header = False, mode = 'a', index = False)
        print('successful')
#         os.remove(url[37:-4])
    except Exception as e:
        print(e)


# In[13]:


count = 0
newest = 618000
for line in html[440000:450000]:
    try:
        # Article Exraction --------------------------------

        print(count)
        url = line.split()[2].decode('UTF-8')
        print(url[len(url)-14:len(url)])
        if url[len(url)-14:len(url)] != 'export.CSV.zip':
            print('not csv', count)
            count += 1
            continue

        wget.download(url, 'E:/fr')

        # opening the zip file in READ mode 
        with ZipFile(url[37:], 'r') as zip: 
            # extracting all the files 
            print('Extracting all the files now...') 
            zip.extractall() 
            print('Done!') 

        # --------------- Pandas and Labeling Section ---------------
        d = pd.read_csv(url[37:-4], sep='\t', header=None)

        d.columns = table_cols

        url_titles = []
        base_url = []
        for i in d['sourceurl'].tolist():
            temp_list = []
            splits = i.split('-')
            temp_list.append(splits[0].split('/')[len(splits[0].split('/')) - 1])
            temp_list.extend(splits[1:])
            url_titles.append(" ".join(temp_list))
            split_url = urlsplit(i)
            base = split_url.netloc
            base_url.append(base)

            cu = []
        for i in range(len(url_titles)):
            cu.append(0)
        for i in range(len(url_titles)):
            for j in range(len(url_titles[i].split()) - 1):
                if url_titles[i].split()[j].lower() in pol_unrest.split():
                    cu[i] = 1
                    break
        
        pu = []
        for i in range(len(url_titles)):
            pu.append(0)
        for i in range(len(url_titles)):
            for j in range(len(url_titles[i].split()) - 1):
                if url_titles[i].split()[j].lower() in labor.split():
                    pu[i] = 1
                    break
                if url_titles[i].split()[j].lower() + ' ' + url_titles[i].split()[j+1].lower() in bilabor.split():
                    pu[i] = 1
                    break

        d['base_url'] = base_url
        d['title'] = url_titles
        d['civil_unrest'] = cu
        d['labor'] = pu
        d = d.drop_duplicates(subset = 'sourceurl')
        d = d.drop_duplicates(subset = 'base_url')
        d = d.drop_duplicates(subset = 'title')

        d = d[["globaleventid", "day", "actor1name", "actor2name", 'numarticles', "avgtone", "actor1geo_fullname", 'actor1geo_lat',
       'actor1geo_long', "actor2geo_fullname", "actiongeo_fullname", "sourceurl", 'base_url', 'title', 'civil_unrest', 'labor']]

        d.to_csv('E:/fr/440000:450000.csv', header = False, mode = 'a', index = False)
        print('successful')
#         os.remove(url[37:-4])
    except Exception as e:
        print(e)


# In[20]:


d = pd.read_csv('E:/fr/500000-550000.csv', header = None)


# In[21]:


d.columns = ["globaleventid", "day", "actor1name", "actor2name", 'numarticles', "avgtone", "actor1geo_fullname", 'actor1geo_lat',
       'actor1geo_long', "actor2geo_fullname", "actiongeo_fullname", "sourceurl", 'base_url', 'title', 'civil_unrest', 'labor']


# In[24]:


d.head()


# In[23]:


d = d[d['labor'] == 1]


# In[12]:


df = pd.read_csv('e:/fr/610000-622608-25 - Copy - Copy.csv')


# In[13]:


df.columns = ["globaleventid", "day", "actor1name", "actor2name", 'numarticles', "avgtone", "actor1geo_fullname", 'actor1geo_lat',
       'actor1geo_long', "actor2geo_fullname", "actiongeo_fullname", "sourceurl", 'base_url', 'title', 'civil_unrest', 'labor', 
             'accept']


# In[14]:


df.head()


# In[25]:


try:
    df = pd.read_csv('e:/fr/610000-622608-25 - Copy - Copy.csv', header = None)
#     df.columns = ["globaleventid", "day", "actor1name", "actor2name", 'numarticles', "avgtone", "actor1geo_fullname", 'actor1geo_lat',
#        'actor1geo_long', "actor2geo_fullname", "actiongeo_fullname", "sourceurl", 'base_url', 'titles', 'accept']
    df.columns = ["globaleventid", "day", "actor1name", "actor2name", 'numarticles', "avgtone", "actor1geo_fullname", 'actor1geo_lat',
       'actor1geo_long', "actor2geo_fullname", "actiongeo_fullname", "sourceurl", 'base_url', 'title', 'civil_unrest', 'labor', 
             'accept']
    # Split up inputs and labels
    train_data, train_labels = df[['base_url', 'title']], df[['accept']]
    dev_data, dev_labels = d[['sourceurl', 'title']], d[['labor']]

    # Check out the shape of the data
    print(' train data shape: ', train_data.shape)
    print('dev data shape: ', dev_data.shape)
    print(' train label shape:', train_labels.shape)
    print('dev label shape:', dev_labels.shape)

    # Initiate empty count vectorizer
    vectorizer = CountVectorizer(analyzer='word')

    # Use the text from the requests to build the feature names and vectorize into a matrix
    vec_train_text = vectorizer.fit_transform(train_data['title'])

    # Vectorize the dev data (already 'fit' the feature names earlier, now we just use .transform)
    vec_dev_text = vectorizer.transform(dev_data['title'].values.astype('U'))

    ## Try Multi NB model:
    NB_baseline = MultinomialNB(alpha=.1)
    NB_baseline.fit(vec_train_text, np.ravel(train_labels))

    ## Try 1-Nearest Neighbor
    knn_baseline = KNeighborsClassifier(n_neighbors=5)
    knn_baseline.fit(vec_train_text, np.ravel(train_labels))

    ## Report Baseline accuracy
    print('Baseline Naive Bayes Accuracy: ', NB_baseline.score(vec_dev_text, dev_labels))

    d['predict'] = NB_baseline.predict(vec_dev_text).tolist()
    
    d = d.drop_duplicates(subset = ['title'], keep='first')

    vd = d[d['predict'] == 1]


#     d = d[['title', 'sourceurl', 'actiongeo_fullname', 'day']]
    
#     d.columns = ['title', 'link',  'snippet', 'date_published']
#     d.to_csv('E:/fr/500000-550000-predict-parsed.csv')
except Exception as e:
    print(e)
    


# In[27]:


# try:
df = pd.read_csv('e:/fr/610000-622608-25 - Copy - Copy.csv', header = None)
#     df.columns = ["globaleventid", "day", "actor1name", "actor2name", 'numarticles', "avgtone", "actor1geo_fullname", 'actor1geo_lat',
#        'actor1geo_long', "actor2geo_fullname", "actiongeo_fullname", "sourceurl", 'base_url', 'titles', 'accept']
df.columns = ["globaleventid", "day", "actor1name", "actor2name", 'numarticles', "avgtone", "actor1geo_fullname", 'actor1geo_lat',
   'actor1geo_long', "actor2geo_fullname", "actiongeo_fullname", "sourceurl", 'base_url', 'title', 'civil_unrest', 'labor', 
         'accept']
# Split up inputs and labels
train_data, train_labels = df[['base_url', 'title']], df[['accept']]
dev_data, dev_labels = d[['sourceurl', 'title']], d[['labor']]

# Check out the shape of the data
print(' train data shape: ', train_data.shape)
print('dev data shape: ', dev_data.shape)
print(' train label shape:', train_labels.shape)
print('dev label shape:', dev_labels.shape)

# Initiate empty count vectorizer
vectorizer = CountVectorizer(analyzer='word')

# Use the text from the requests to build the feature names and vectorize into a matrix
vec_train_text = vectorizer.fit_transform(train_data['title'])

# Vectorize the dev data (already 'fit' the feature names earlier, now we just use .transform)
vec_dev_text = vectorizer.transform(dev_data['title'].values.astype('U'))

## Try Multi NB model:
NB_baseline = MultinomialNB(alpha=.1)
NB_baseline.fit(vec_train_text, np.ravel(train_labels))

## Try 1-Nearest Neighbor
knn_baseline = KNeighborsClassifier(n_neighbors=5)
knn_baseline.fit(vec_train_text, np.ravel(train_labels))

## Report Baseline accuracy
print('Baseline Naive Bayes Accuracy: ', NB_baseline.score(vec_dev_text, dev_labels))

d['predict'] = NB_baseline.predict(vec_dev_text).tolist()

d = d.drop_duplicates(subset = ['title'], keep='first')

vd = d[d['predict'] == 1]


#     d = d[['title', 'sourceurl', 'actiongeo_fullname', 'day']]
    
#     d.columns = ['title', 'link',  'snippet', 'date_published']
#     d.to_csv('E:/fr/500000-550000-predict-parsed.csv')
# except Exception as e:
#     print(e)
    


# In[50]:


vd.to_csv('484000:485000-predict-parsed.csv')


# In[52]:


d.head()


# In[51]:


vd['title'].tolist()


# # Continuous Code

# In[ ]:


len(html)


# In[ ]:


newest = count = 624060
while True:
    for line in html[newest:newest + 3]:
        try:
            # Article Exraction --------------------------------
            print(count)
            url = line.split()[2].decode('UTF-8')
            print(url[len(url)-14:len(url)])
            if url[len(url)-14:len(url)] != 'export.CSV.zip':
                print('not csv', count)
                count += 1
                continue

            wget.download(url, 'E:/fr')

            # opening the zip file in READ mode 
            with ZipFile(url[37:], 'r') as zip: 
                # extracting all the files 
                print('Extracting all the files now...') 
                zip.extractall() 
                print('Done!') 

            # --------------- Pandas and Labeling Section ---------------
            d = pd.read_csv(url[37:-4], sep='\t', header=None)

            table_cols = ['globaleventid', 'day', 'monthyear', 'year', 'fractiondate',
           'actor1code', 'actor1name', 'actor1countrycode', 'actor1knowngroupcode',
           'actor1ethniccode', 'actor1religion1code', 'actor1religion2code',
           'actor1type1code', 'actor1type2code', 'actor1type3code', 'actor2code',
           'actor2name', 'actor2countrycode', 'actor2knowngroupcode',
           'actor2ethniccode', 'actor2religion1code', 'actor2religion2code',
           'actor2type1code', 'actor2type2code', 'actor2type3code', 'isrootevent',
           'eventcode', 'eventbasecode', 'eventrootcode', 'quadclass',
           'goldsteinscale', 'nummentions', 'numsources', 'numarticles', 'avgtone',
           'actor1geo_type', 'actor1geo_fullname', 'actor1geo_countrycode',
           'actor1geo_adm1code', 'actor1geo_adm2code', 'actor1geo_lat',
           'actor1geo_long', 'actor1geo_featureid', 'actor2geo_type',
           'actor2geo_fullname', 'actor2geo_countrycode', 'actor2geo_adm2code',
           'actor2geo_adm2code.1', 'actor2geo_lat', 'actor2geo_long',
           'actor2geo_featureid', 'actiongeo_type', 'actiongeo_fullname',
           'actiongeo_countrycode', 'actiongeo_adm1code', 'actiongeo_adm2code',
           'actiongeo_lat', 'actiongeo_long', 'actiongeo_featureid', 'dateadded',
           'sourceurl']

            d.columns = table_cols

            url_titles = []
            base_url = []
            for i in d['sourceurl'].tolist():
                temp_list = []
                splits = i.split('-')
                temp_list.append(splits[0].split('/')[len(splits[0].split('/')) - 1])
                temp_list.extend(splits[1:])
                url_titles.append(" ".join(temp_list))
                split_url = urlsplit(i)
                base = split_url.netloc
                base_url.append(base)

                cu = []
            for i in range(len(url_titles)):
                cu.append(0)
            for i in range(len(url_titles)):
                for j in range(len(url_titles[i].split()) - 1):
                    if url_titles[i].split()[j].lower() in pol_unrest.split():
                        cu[i] = 1
                        break

            pu = []
            for i in range(len(url_titles)):
                pu.append(0)
            for i in range(len(url_titles)):
                for j in range(len(url_titles[i].split()) - 1):
                    if url_titles[i].split()[j].lower() in labor.split():
                        pu[i] = 1
                        break
                    if url_titles[i].split()[j].lower() + ' ' + url_titles[i].split()[j+1].lower() in bilabor.split():
                        pu[i] = 1
                        break

            d['base_url'] = base_url
            d['title'] = url_titles
            d['civil_unrest'] = cu
            d['labor'] = pu
            d = d.drop_duplicates(subset = 'sourceurl')
            d = d.drop_duplicates(subset = 'title')

            d = d[["globaleventid", "day", "actor1name", "actor2name", 'numarticles', "avgtone", "actor1geo_fullname", 'actor1geo_lat',
           'actor1geo_long', "actor2geo_fullname", "actiongeo_fullname", "sourceurl", 'base_url', 'title', 'civil_unrest', 'labor']]

            # Article Classification and Prediction Section -------------------------

            df = pd.read_csv('e:/fr/auto-test-train.csv',header = None)
            df.columns = ["globaleventid", "day", "actor1name", "actor2name", 'numarticles', "avgtone", "actor1geo_fullname", 'actor1geo_lat',
               'actor1geo_long', "actor2geo_fullname", "actiongeo_fullname", "sourceurl", 'base_url', 'titles', 'accept']

            # Split up inputs and labels
            train_data, train_labels = df[['base_url', 'titles']], df[['accept']]
            dev_data, dev_labels = d[['sourceurl', 'title']], d[['civil_unrest']]

            # Check out the shape of the data
            print(' train data shape: ', train_data.shape)
            print('dev data shape: ', dev_data.shape)
            print(' train label shape:', train_labels.shape)
            print('dev label shape:', dev_labels.shape)

            # Initiate empty count vectorizer
            vectorizer = CountVectorizer(analyzer='word')

            # Use the text from the requests to build the feature names and vectorize into a matrix
            vec_train_text = vectorizer.fit_transform(train_data['titles'])

            # Vectorize the dev data (already 'fit' the feature names earlier, now we just use .transform)
            vec_dev_text = vectorizer.transform(dev_data['title'].values.astype('U'))

            ## Try Multi NB model:
            NB_baseline = MultinomialNB(alpha=.1)
            NB_baseline.fit(vec_train_text, np.ravel(train_labels))

            ## Report Baseline accuracy
            print('Baseline Naive Bayes Accuracy: ', NB_baseline.score(vec_dev_text, dev_labels))

            d['predict'] = NB_baseline.predict(vec_dev_text).tolist()

            d = d.drop_duplicates(subset = ['title'], keep='first')

            d = d[d['predict'] == 1]

            d = d[['title', 'sourceurl', 'actiongeo_fullname', 'day']]
            
            print('titles', d['title'].tolist())
            # df.columns = {'titles': 'title', 'sourceurl': 'link', 'actiongeo_fullname': 'snippet', 'day':'date_published'}
            d.columns = ['title', 'link',  'snippet', 'date_published']

            dicti = {'api_key': 'YlvDBuF6aQBqFo2ePGK2rw', 'articles': d.to_dict('records')}
            dicti

            # importing the requests library 
            import requests 

            # defining the api-endpoint  
            API_ENDPOINT = "http://cbde401d49bb.ngrok.io/articles/civil_unrest"

            # your API key here 
            API_KEY = "YlvDBuF6aQBqFo2ePGK2rw"


            data = {'api_dev_key': API_KEY} 

            # sending post request and saving response as response object 
            # r = requests.post(url = API_ENDPOINT, data = data, json = 'E:/fr/auto-test.json') 
            r = requests.post(url = API_ENDPOINT, data = data, json = dicti)   
            # r = requests.post(url = API_ENDPOINT, data = data, json = dicti)   
            newest = newest + 3 
            # extracting response text  
            response = r.text 
            print("Response text is:%s"%response) 

            d.to_csv('E:/fr/3-15-test.csv', header = False, mode = 'a', index = False)
        except Exception as e:
            count += 1
            print('error_____________________________________________', e)
        time.sleep(9)


# In[ ]:




